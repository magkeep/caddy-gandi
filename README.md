# caddy-gandi
A simple Docker image with the Gandi DNS provider module : [https://github.com/caddy-dns/gandi](https://github.com/caddy-dns/gandi).

Available at `registry.gitlab.com/magkeep/caddy-gandi`.